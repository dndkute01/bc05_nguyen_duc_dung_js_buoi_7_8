var numberArr = [];
function themSo() {
  var number = document.querySelector("#txt-number").value * 1;

  // numberArr.push(number);

  if (document.querySelector("#txt-number").value == "") {
    // document.getElementsByTagName("button").disabled = "true";
  } else {
    // document.getElementsByTagName("button").disabled = "false";
    numberArr.push(number);
    document.querySelector("#txt-number").value = "";
    document.querySelector("#ketQua").innerHTML = numberArr;
  }

  return numberArr;
}
// Tính tổng
function tinhTong() {
  numberArr = themSo();
  var tongSoDuong = 0;

  for (var index = 0; index < numberArr.length; index++) {
    var currentNumber = numberArr[index];
    if (currentNumber > 0) {
      tongSoDuong += currentNumber;
    }
  }

  document.querySelector(
    "#ketQua1"
  ).innerHTML = `Tổng số dương: ${tongSoDuong}`;
}
// Đếm số dương

function demSoDuong() {
  numberArr = themSo();
  var demSoDuong = 0;
  var soDuong = [];
  for (var index = 0; index < numberArr.length; index++) {
    var currentNumber = numberArr[index];
    if (currentNumber > 0) {
      demSoDuong++;
      soDuong.push(currentNumber);
    }
  }
  document.querySelector("#ketQua2").innerHTML = `Số dương: ${demSoDuong}`;
  return soDuong;
}
// Tìm số nhỏ nhất

function timSoNhoNhat() {
  numberArr = themSo();
  var soNhoNhat = numberArr[0];

  for (var index = 0; index < numberArr.length; index++) {
    var currentNumber = numberArr[index];

    if (currentNumber < soNhoNhat) {
      soNhoNhat = currentNumber;
    }
  }

  document.querySelector("#ketQua3").innerHTML = `Số nhỏ nhất: ${soNhoNhat}`;
}

// Tìm số dương nhỏ nhất

function timSoDuongNhoNhat() {
  var soDuong = demSoDuong();
  var soDuongNhoNhat = soDuong[0];
  for (var index = 0; index < soDuong.length; index++) {
    var soDuongHienTai = soDuong[index];
    if (soDuongHienTai < soDuongNhoNhat) {
      soDuongNhoNhat = soDuongHienTai;
    }
  }
  document.getElementById(
    "ketQua4"
  ).innerHTML = `Số dương nhỏ nhất: ${soDuongNhoNhat}`;
}

// Tìm số chẵn cuối cùng

function timSoChan() {
  var numberArr = themSo();
  var soChan = [];
  var soChanCuoiCung;
  for (var index = 0; index < numberArr.length; index++) {
    var soHienTai = numberArr[index];

    if (soHienTai % 2 == 0) {
      soChan.push(soHienTai);
    } else {
      soChanCuoiCung = -1;
    }
  }
  for (var i = 0; i < soChan.length; i++) {
    soChanCuoiCung = soChan[i];
    if (i == soChan.length - 1) {
      soChanCuoiCung = soChan[i];
    }
  }
  document.getElementById(
    "ketQua5"
  ).innerHTML = `Số chẵn cuối cùng: ${soChanCuoiCung}`;
}

/*
 for ( var index = numberArr.length -1; index >= 0; index -- ){

}

*/

// Đổi chỗ

function doiCho() {
  numberArr = themSo();
  console.log("numberArr: ", numberArr);
  var p1 = document.querySelector("#viTri1").value * 1;
  console.log("p1: ", p1);
  var p2 = document.querySelector("#viTri2").value * 1;
  console.log("p2: ", p2);

  for (var index = 0; index < numberArr.length; index++) {
    var value = numberArr[index];

    if (index == p1) {
      value = numberArr[p1];

      // console.log("value index = 1: ", value);
    }
    if (index == p2) {
      var a = numberArr[p2];
      // console.log("value a: ", a);
      numberArr[p2] = numberArr[p1];
      // console.log("numberArr khi p2: ", numberArr);
      numberArr[p1] = a;
      // console.log("numberArr: ", numberArr);
    }
  }

  document.getElementById("ketQua6").innerHTML = numberArr;
}

// Sắp xếp tăng dần
function sapXep() {
  var numberArr = themSo();
  var n;
  for (var i = 0; i < numberArr.length - 1; i++) {
    for (var j = i + 1; j < numberArr.length; j++) {
      if (numberArr[i] > numberArr[j]) {
        var temp = numberArr[i];
        numberArr[i] = numberArr[j];
        numberArr[j] = temp;
      }
    }
    document.querySelector("#ketQua7").innerHTML = numberArr;
  }
  // Hàm đổi vị trí 2 phần tử
  // function swap(a, b) {
  //   var temp = a;
  //   a = b;
  //   b = temp;
  //   var z = [a, b];
  //   return z;
  // }
}

// Tìm số nguyên tố đầu tiên trong mảng
function timSo() {
  var numberArr = themSo();

  for (var index = 0; index < numberArr.length; index++) {
    var curNum = numberArr[index];
    var checkSNT = kiem_tra_snt(curNum);
    if (checkSNT == true) {
      var sNT = curNum;
      console.log("sNT: ", sNT);
      break;
    }
  }
  document.getElementById(
    "ketQua8"
  ).innerHTML = `Số nguyên tố đầu tiên: ${sNT}`;
}

function kiem_tra_snt(n) {
  // Biến cờ hiệu
  var flag = true;

  // Nếu n bé hơn 2 tức là không phải số nguyên tố
  if (n < 2) {
    flag = false;
  } else if (n == 2) {
    flag = true;
  } else if (n % 2 == 0) {
    flag = false;
  } else {
    // lặp từ 3 tới n-1 với bước nhảy 1
    for (var i = 3; i < n - 1; i++) {
      if (n % i == 0) {
        flag = false;
        break;
      }
    }
  }
  return flag;
}

// Đếm số nguyên
function demSo() {
  var numberArr = themSo();
  var count = 0;
  for (var i = 0; i < numberArr.length; i++) {
    var curNum = numberArr[i];
    if (Number.isInteger(curNum) == true) {
      count++;
    }
  }
  document.getElementById("ketQua9").innerHTML = `Đếm số nguyên: ${count}`;
}

// So sánh số lượng số âm và số dương

function soSanh() {
  var numberArr = themSo();
  var positiveNum = 0;
  var negativeNum = 0;
  var result = "";
  for (var i = 0; i < numberArr.length; i++) {
    var curNum = numberArr[i];
    if (curNum > 0) {
      positiveNum++;
    }
    if (curNum < 0) {
      negativeNum++;
    }
  }
  if (positiveNum > negativeNum) {
    result = `Số dương > Số âm`;
  }
  if (positiveNum < negativeNum) {
    result = `Số dương < Số âm`;
  }
  if (positiveNum == negativeNum) {
    result = `Số dương = Số âm`;
  }

  document.getElementById("ketQua10").innerHTML = result;
}
